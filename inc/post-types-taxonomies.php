<?php
/**
 * neoo_al Custom Post Types and Taxonomies
 *
 * @package neoo_al
 */

// Register Custom Post Type
function create_homepage_slider() {

	$labels = array(
		'name'                => _x( 'Homepage Sliders', 'Post Type General Name', 'neoo_al' ),
		'singular_name'       => _x( 'Homepage Slider', 'Post Type Singular Name', 'neoo_al' ),
		'menu_name'           => __( 'Homepage Slider', 'neoo_al' ),
		'parent_item_colon'   => __( 'Parent Slide', 'neoo_al' ),
		'all_items'           => __( 'All Sliders', 'neoo_al' ),
		'view_item'           => __( 'View Slide', 'neoo_al' ),
		'add_new_item'        => __( 'Add New Slide', 'neoo_al' ),
		'add_new'             => __( 'Add New', 'neoo_al' ),
		'edit_item'           => __( 'Edit Slide', 'neoo_al' ),
		'update_item'         => __( 'Update Slide', 'neoo_al' ),
		'search_items'        => __( 'Search Slide', 'neoo_al' ),
		'not_found'           => __( 'Not found', 'neoo_al' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'neoo_al' ),
	);
	$rewrite = array(
		'slug'                => 'slide',
		'with_front'          => true,
		'pages'               => true,
		'feeds'               => false,
	);
	$args = array(
		'label'               => __( 'homepage_slider', 'neoo_al' ),
		'description'         => __( 'Updates slides on homepage carousel.', 'neoo_al' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'thumbnail', 'custom-fields', 'page-attributes', ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => true,
		'publicly_queryable'  => true,
		'rewrite'             => $rewrite,
		'capability_type'     => 'page',
	);
	register_post_type( 'homepage_slider', $args );

}

// Hook into the 'init' action
add_action( 'init', 'create_homepage_slider', 0 );
