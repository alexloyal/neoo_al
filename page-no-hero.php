<?php
/*
Template Name: No Hero
*/

/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package neoo_al
 */

get_header(); ?>

<!-- page-no-hero -->
 

 
	<div class="inside-content">

		<div id="primary" class="content-area col-md-9">
			<main id="main" class="site-main col-md-12" role="main">
					<header class="entry-header">
						<?php the_title( '<h1 class="no-hero-entry-title">', '</h1>' ); ?>
					</header><!-- .entry-header -->
				<?php while ( have_posts() ) : the_post(); ?>


				 




					<?php get_template_part( 'content', 'page' ); ?>

				

				<?php endwhile; // end of the loop. ?>

			</main><!-- #main -->
		</div><!-- #primary -->

	</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>
