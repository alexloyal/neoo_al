<?php
/**
 * @package neoo_al
 */
?>
<!-- home-pane -->
<div class="home-slider-wrapper">
				<div class="home-slider col-md-12" data-home-slider="true" data-wrap="circular">
					<div class="home-slider-item-container">
					<?php 
					// WP_Query arguments
						$args = array (
						'post_type'              => 'homepage_slider',
						'post_status'            => 'published',
						'order'                  => 'ASC',
						'orderby'                => 'menu_order',
						);

						// The Query
						$hpSlidesQuery = new WP_Query( $args );

						if ($hpSlidesQuery->have_posts()) : 
						while ($hpSlidesQuery->have_posts()) : 
						$hpSlidesQuery->the_post(); 

					?>

						 
						<span class="home-slider-item col-md-3">
							 
							<?php echo get_the_post_thumbnail($post->ID, 'homepage-slide'); ?>
							<div class="slide-caption blue background translucent col-sm-3">
								<h3><?php the_title(); ?></h3>
								<?php the_content(); ?>
								
								
							</div>
						</span>
					

					<?php endwhile; wp_reset_postdata();  ?>
							
					<?php endif; ?>
					 
				 	</div>
	                <a data-home-slider-control="true" data-target="-=1" href="#" class="home-slider-control-prev">&lsaquo;</a>
                	<a data-home-slider-control="true" data-target="+=1" href="#" class="home-slider-control-next">&rsaquo;</a>
	              
	              <div data-home-slider-pagination="true" class="home-slider-pagination col-md-12"></div>
				</div>
	 		</div>
	  
