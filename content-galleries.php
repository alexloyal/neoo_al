<?php
/**
 * The template used for displaying page content in page-galleries.php
 *
 * @package neoo_al
 */
?>
<!-- begin content-galleries -->
<article id="post-<?php the_ID(); ?>" <?php post_class('col-md-12 gallery-format'); ?>>
	

<!-- content-galleries -->
	<div class="entry-content col-md-12">
		<?php ?>
		<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail('blog-thumb'); ?></a>
		 <h3><?php the_title(); ?></h3>
	</div><!-- .entry-content -->

 
	 
</article><!-- #post-## -->
