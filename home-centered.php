
<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package neoo_al
 */

get_header(); ?>
<!-- home-centered -->
	<div id="primary" class="content-area container">
		<main id="main" class="site-main row" role="main">
			


			 
		<div class="panes">	
			<div class="home-slider-wrapper">
						<div class="home-slider-panes col-md-12" data-home-slider="true" data-wrap="circular">
							<div class="home-slider-item-container roundCorners">
							<?php 
							// WP_Query arguments
								$args = array (
								'post_type'              => 'homepage_slider',
								'post_status'            => 'published',
								'order'                  => 'ASC',
								'orderby'                => 'menu_order',
								);
		
								// The Query
								$hpSlidesQuery = new WP_Query( $args );
								 
								if ($hpSlidesQuery->have_posts()) : 
								$post = $posts[0]; $c=0;
								while ($hpSlidesQuery->have_posts()) : 
								$hpSlidesQuery->the_post(); 
		
							?>
								<?php $c++; if( $c == 1) : ?>
									 
									<span class="home-slider-item col-md-9">
										 
										<?php echo get_the_post_thumbnail($post->ID, 'homepage-slide'); ?>
										<div class="slide-caption-panes blue roundCorners background translucent">
											<h3><?php the_title(); ?></h3>
											<?php the_content(); ?>
											
											
										</div>
									</span>
								
								<?php else : ?>
									<span class="home-slider-item col-md-3">
										 
										<?php echo get_the_post_thumbnail($post->ID, 'homepage-slide-short'); ?>
										<div class="slide-caption-panes roundCorners blue background translucent">
											<h3><?php the_title(); ?></h3>
											<?php the_content(); ?>
											
											
										</div>
									</span>
								<?php endif; ?>	

							<?php endwhile; wp_reset_postdata();  ?>
									
							<?php endif; ?>
							 
						 	</div>
			                
			              
			             
						</div>
			 		</div>
		</div>
	  
		</main><!-- #main -->
	</div><!-- #primary -->

	<div id="" class="content-area container">

		<div class="home-bottom row">
			<?php if ( ! dynamic_sidebar( 'home-bottom' ) ) : ?>
			<?php endif; // end sidebar widget area ?>
		</div>

	</div><!-- #secondary -->

<?php get_footer(); ?>
