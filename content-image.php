<?php
/**
 * The template used for displaying page content in page-image.php
 *
 * @package neoo_al
 */
?>
<!-- begin content-image -->
<article id="post-<?php the_ID(); ?>" <?php post_class('col-md-12'); ?>>
	
<!-- content-image -->
	<div class="entry-content">
		 
		<?php the_content(); ?> 
		 <?php edit_post_link( __( 'Edit', 'neoo_al' ), '<span class="edit-link">', '</span>' ); ?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php if ( 'post' == get_post_type() ) : // Hide category and tag text for pages on Search ?>
			<?php
				/* translators: used between list items, there is a space after the comma */
				$categories_list = get_the_category_list( __( ', ', 'neoo_al' ) );
				if ( $categories_list && neoo_al_categorized_blog() ) :
			?>
			<span class="cat-links">
				<?php printf( __( 'Posted in %1$s', 'neoo_al' ), $categories_list ); ?>
			</span>
			<?php endif; // End if categories ?>

			<?php
				/* translators: used between list items, there is a space after the comma */
				$tags_list = get_the_tag_list( '', __( ', ', 'neoo_al' ) );
				if ( $tags_list ) :
			?>
			<span class="tags-links">
				<?php printf( __( 'Tagged %1$s', 'neoo_al' ), $tags_list ); ?>
			</span>
			<?php endif; // End if $tags_list ?>
		<?php endif; // End if 'post' == get_post_type() ?>

		<?php if ( ! post_password_required() && ( comments_open() || '0' != get_comments_number() ) ) : ?>
		<span class="comments-link"><?php comments_popup_link( __( 'Leave a comment', 'neoo_al' ), __( '1 Comment', 'neoo_al' ), __( '% Comments', 'neoo_al' ) ); ?></span>
		<?php endif; ?>

		<?php edit_post_link( __( 'Edit', 'neoo_al' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-footer -->
	 
</article><!-- #post-## -->
