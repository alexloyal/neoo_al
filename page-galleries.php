<?php
/*
Template Name: NEOO Photo Galleries
*/

/**
 * The template for displaying a landing page for galleries.	
 *
 * @package neoo_al
 */

get_header(); ?>

<div class="content-hero" style="background-image:url('<?php 
												echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); 
											 ?>');" class="">
			<header class="entry-header">
				<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
			</header><!-- .entry-header -->								 

</div>											 

	<div class="inside-content">
				<div id="primary" class="content-area container">
					<main id="main" class="site-main row" role="main">
						<!-- page-galleries -->
			<?php 
						// WP_Query arguments
							$args = array (
								'category_name'          => 'galleries',
							);

							// The Query
							$galleryQuery = new WP_Query( $args );

							// The Loop
							if ( $galleryQuery->have_posts() ) {
								while ( $galleryQuery->have_posts() ) {
									$galleryQuery->the_post();
									// do something
									get_template_part( 'content', 'galleries' ); 

								}




							} else {
								// no posts found
							}

								// If comments are open or we have at least one comment, load up the comment template
								if ( comments_open() || '0' != get_comments_number() ) :
									comments_template();
								endif;
						 
							// Restore original Post Data
							wp_reset_postdata();
						?>
						 
					</main><!-- #main -->
				</div><!-- #primary -->


	</div>

<?php get_footer(); ?>
