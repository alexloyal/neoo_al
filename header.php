<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package neoo_al
 */
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="description" content="<?php bloginfo( 'description' ); ?>">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<style>
            body {
                padding-top: 50px;
                padding-bottom: 20px;
            }
        </style>
<?php wp_head(); ?>
<!--[if gte IE 9]>
  <style type="text/css">
    .gradient {
       filter: none;
    }
  </style>
<![endif]-->
</head> 

<body <?php body_class(''); ?>>
<div id="page" class="hfeed site container-fluid">
		<header id="masthead" class="site-header row" role="banner">
			<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'neoo_al' ); ?></a>
				<div class="site-branding col-md-12 col-xs-12">
					<h1 class="site-title col-md-2 col-xs-2"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="text-hide" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
					
					<div class="col-md-10 col-xs-6 top-tools">
						<div class="col-md-5 col-xs-5 mobilefloat"><?php wp_nav_menu( array( 
												'theme_location' => 'top',
			          							'menu_class' => 'list-inline menu' ) ); ?></div>
						<div class="col-md-3 col-xs-3 mobilefloat"><?php wp_nav_menu( array( 
													'theme_location' => 'social',
				          							'menu_class' => 'list-inline menu',
				          							'link_before' => '<span class="',
				          							'link_after' => '-icon"></span>' ) ); ?></div>
						<div id="search" class="col-md-4 col-xs-4 mobilefloat"><?php get_search_form(); ?></div>
					</div>

					<div class="col-md-10 navigationtools col-xs-12">
						<div class="tagline col-md-12 col-xs-12">
									      		
									      		<map name="clf-link">
									      			<area shape="rect" alt="Conservation Law Foundation" title="Conservation Law Foundation" coords="571,631,42,42" href="http://www.clf.org" target="_blank">
									      		</map>
					
									      		<a href="http://www.clf.org" target="_blank" title="Conservation Law Foundation">
									      			<img src="<?php bloginfo('url'); ?>/wp-content/themes/neoo_al/images/neoo-tagline.png" usemap="#clf-link" width="632" height="43" alt="<?php bloginfo('description'); ?>" />
									      		</a>
									      			
					
					
					
									      		<h3 class="text-hide" ><span><?php bloginfo('description'); ?></span></h3>
									      	</div>	
										
										<div class="container col-md-12 navbar navbar-inverse" role="navigation"> 
											<div class="navbar-header">
									          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
									            <span class="sr-only">Toggle navigation</span>
									            <span class="icon-bar"></span>
									            <span class="icon-bar"></span>
									            <span class="icon-bar"></span>
									          </button>
									          <!-- <a class="navbar-brand"></a> -->
									        </div>
									        <div class="navbar-collapse collapse">
									          <?php wp_nav_menu( array( 	
									          							'theme_location' => 'nav',
									          							'menu_class' => 'list-inline menu',
									          							'container_class' => 'main-navigation'
					
									           ) ); ?>
					
									        </div><!--/.navbar-collapse -->
					
									     </div>
					</div>
				      	
					</div>
				  
			
				
			      

		</header><!-- #masthead -->
				

 

		<!-- #site-navigation -->


<div id="content" class="site-content row">

	<div id="breadcrumb-navigation" class="col-md-12">
		<?php the_breadcrumb(); ?>
	</div>