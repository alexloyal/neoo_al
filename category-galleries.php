<?php
/**
 * The template for displaying a landing page for galleries.	
 *
 * @package neoo_al
 */

get_header(); ?>

	<div id="primary" class="content-area container row">
		<main id="main" class="site-main col-md-12" role="main">
				<header class="entry-header col-md-12">
					<?php single_cat_title( '<h1 class="entry-title single">', '</h1>' ); ?>
				</header><!-- .entry-header -->
				<!-- category-galleries -->
<?php 
			// WP_Query arguments
				$args = array (
					'category_name'          => 'galleries',
				);

				// The Query
				$galleryQuery = new WP_Query( $args );

				// The Loop
				if ( $galleryQuery->have_posts() ) {
					while ( $galleryQuery->have_posts() ) {
						$galleryQuery->the_post();
						// do something
						get_template_part( 'content', 'galleries' ); 

					}




				} else {
					// no posts found
				}

					// If comments are open or we have at least one comment, load up the comment template
					if ( comments_open() || '0' != get_comments_number() ) :
						comments_template();
					endif;
			 
				// Restore original Post Data
				wp_reset_postdata();
			?>
			 
		</main><!-- #main -->
	</div><!-- #primary -->
 
<?php get_footer(); ?>
