<?php
/**
 * The Sidebar containing the main widget areas.
 *
 * @package neoo_al
 */
?>
	<div id="secondary" class="widget-area col-md-3" role="complementary">
		<?php  ?>

		<?php 
		/*	global $post;
			$pageId = (string)$post->post_name;
			switch ($pageId) {
				case 'about-cashes-ledge':
					dynamic_sidebar('cashes-ledge-sidebar' );
					break;
				case 'about':
					dynamic_sidebar('about-us-sidebar' );
					break;
				case 'contact':
					dynamic_sidebar('contact-us-sidebar' );
					break;
				case 'clf':	
					dynamic_sidebar('clf-sidebar' );
					break;
				case 'brian-skerry':	
					dynamic_sidebar('brian-skerry-sidebar' );
					break;
				case 'take-action':	
					dynamic_sidebar('take-action-sidebar' );
					break;
					default:
					dynamic_sidebar( 'sidebar-1' );

				break;
				 
			} */

			dynamic_sidebar( 'sidebar-1' );

			?>

			<aside class="widget">

			<h3 class="blog-sidebar-title">Blog</h3>
			<?php
			// WP_Query arguments
			$args = array (
				'post_type'              => 'post',
				'posts_per_page'         => '3',
			);

			// The Query
			$blogSidebar = new WP_Query( $args );

			// The Loop
			if ( $blogSidebar->have_posts() ) {
				while ( $blogSidebar->have_posts() ) {
					$blogSidebar->the_post();
					// do something

					?>
					<h3 class="sidebar-headline"><a href="<?php the_permalink(); ?>" title="<?php the_title( ); ?>">
						<?php the_title( ); ?>
					</a></h3>	
					<?php

					echo get_the_post_thumbnail($post->ID, 'blog-thumb');
					
				}
			} else {
				// no posts found
			}

			// Restore original Post Data
			wp_reset_postdata();

			?>
			</aside>

			<aside class="widget">
				<h3 class="widget-title">Follow NEOO</h3>
				<?php wp_nav_menu( array( 
													'theme_location' => 'social',
				          							'menu_class' => 'list-inline menu',
				          							'link_before' => '<span class="',
				          							'link_after' => '-icon"></span>' ) ); ?>

			</aside>

			<?php



		 dynamic_sidebar( 'sidebar-after-blog' );
		 ?>

	</div><!-- #secondary -->
