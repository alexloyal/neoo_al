<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package neoo_al
 */
?>
<!-- content-page -->
<article id="post-<?php the_ID(); ?>" <?php post_class('col-md-12'); ?>>


	<div class="entry-content">
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'neoo_al' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->
	<footer class="entry-footer">
		<?php edit_post_link( __( 'Edit', 'neoo_al' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
