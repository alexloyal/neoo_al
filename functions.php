<?php
/**
 * neoo_al functions and definitions
 *
 * @package neoo_al
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

if ( ! function_exists( 'neoo_al_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function neoo_al_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on neoo_al, use a find and replace
	 * to change 'neoo_al' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'neoo_al', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	add_image_size( 'homepage-slide', 1170, 590, array( 'center', 'center' ) );
	add_image_size( 'blog-thumb', 407, 269, array( 'center', 'center' ) );
	add_image_size( 'homepage-slide-short', 640, 423, array( 'center', 'center' ) );
	add_image_size( 'blog-hero', 768, 508, array( 'center', 'center' ) );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'nav' => __( 'Nav Menu', 'neoo_al_nav' ),
		'top' => __( 'Top Menu', 'neoo_al_top' ),
		'social' => __( 'Social Menu', 'neoo_al_social' ),
		'bottom' => __('Bottom Menu', 'neoo_al_bottom')
	) );
	
	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link','gallery'
	) );

	// Setup the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'neoo_al_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // neoo_al_setup
add_action( 'after_setup_theme', 'neoo_al_setup' );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function neoo_al_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'neoo_al_sidebar' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Homepage Bottom', 'neoo_al_home_bottom' ),
		'id'            => 'home-bottom',
		'description'   => '',	
		'before_widget' => '<aside id="%1$s" class="widget col-md-4 %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title shadow">',
		'after_title'   => '</h3>',
	));

	register_sidebar( array(
		'name'          => __( 'Footer', 'neoo_al_footer_widgets' ),
		'id'            => 'footer-widgets',
		'description'   => '',	
		'before_widget' => '<aside id="%1$s" class="widget col-md-4 %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	));
	/*	register_sidebar( array(
		'name'          => __( 'Cashes Ledge Sidebar', 'neoo_al_cashes_ledge_sidebar' ),
		'id'            => 'cashes-ledge-sidebar',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
		register_sidebar( array(
		'name'          => __( 'About Us Sidebar', 'neoo_al_about_sidebar' ),
		'id'            => 'about-us-sidebar',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
		register_sidebar( array(
		'name'          => __( 'Contact Us Sidebar', 'neoo_al_contact_sidebar' ),
		'id'            => 'contact-us-sidebar',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
		register_sidebar( array(
		'name'          => __( 'CLF Sidebar', 'neoo_al_clf_sidebar' ),
		'id'            => 'clf-sidebar',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

		register_sidebar( array(
		'name'          => __( 'Brian Skerry Sidebar', 'neoo_al_brian_skerry_sidebar' ),
		'id'            => 'brian-skerry-sidebar',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
		register_sidebar( array(
		'name'          => __( 'Take Action Sidebar', 'neoo_al_take_action_sidebar' ),
		'id'            => 'take-action-sidebar',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) )*/
	register_sidebar( array(
		'name'          => __( 'Sidebar After Blog', 'neoo_al_sidebar_after_blog' ),
		'id'            => 'sidebar-after-blog',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'neoo_al_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function neoo_al_scripts() {
	

	wp_enqueue_style('boostrap', get_template_directory_uri() . '/css/bootstrap-theme.min.css');

	wp_enqueue_style('google-web-fonts', 'http://fonts.googleapis.com/css?family=Arbutus+Slab|Quattrocento+Sans:400,400italic,700italic,700|Sanchez:400italic,400|Raleway:200');
	
	wp_enqueue_style( 'neoo_al-style', get_stylesheet_uri() );

	wp_deregister_script('jquery');
    
    wp_register_script('jquery', "http" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . "://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js", false, null);
    
    wp_enqueue_script('jquery');

	wp_enqueue_script( 'neoo_al-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );

	wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js', array(), '2.6.2', false );

	wp_enqueue_script('boostrap', get_template_directory_uri() . '/js/vendor/bootstrap.min.js', array(), '3.1.1', true);

	wp_enqueue_script( 'neoo_al-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	wp_enqueue_script('jcarousel', get_template_directory_uri() . '/js/jquery.jcarousel.min.js', array(),'0.3.1', true);

	wp_enqueue_script('jquery-lightbox', get_template_directory_uri() . '/js/jquery.colorbox-min.js', array(),'1.5.14', true);

	wp_enqueue_script('jquery-touch-wipe', get_template_directory_uri() . '/js/vendor/jquery.touchwipe.min.js', array(), '1.1.1', true);

	wp_enqueue_script('slidesjs', get_template_directory_uri() . '/js/vendor/jquery.slides.min.js', array(),'3.0.4', true );

	wp_enqueue_script('responsiveslides', get_template_directory_uri() . '/js/vendor/responsiveslides.min.js', array(), '1.54', true);

	wp_enqueue_script( 'neoo_al_triggers', get_template_directory_uri() . '/js/triggers.js', array('jquery'), '1.0', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'neoo_al_scripts' );


// Breadcrumb navigation
// http://cazue.com/articles/wordpress-creating-breadcrumbs-without-a-plugin-2013

function the_breadcrumb() {
	global $post;
    echo '<ul id="breadcrumbs">';
    if (!is_home()) {
        echo '<li><a href="';
        echo get_option('home');
        echo '">';
        echo 'Home';
        echo '</a></li><li class="separator"> / </li>';
        if (is_category() || is_single()) {
            echo '<li>';
            the_category(' </li><li class="separator"> / </li><li> ');
            if (is_single()) {
                echo '</li><li class="separator"> / </li><li>';
                the_title();
                echo '</li>';
            }
        } elseif (is_page()) {
            if($post->post_parent){
				$anc = get_post_ancestors( $post->ID );
				$title = get_the_title();
				foreach ( $anc as $ancestor ) {
					$output = '<li><a href="'.get_permalink($ancestor).'" title="'.get_the_title($ancestor).'">'.get_the_title($ancestor).'</a></li> <li class="separator">/</li>';
				}
				echo $output;
				echo '<strong title="'.$title.'"> '.$title.'</strong>';
			} else {
				echo '<li><strong> '.get_the_title().'</strong></li>';
			}
        }
    }
    elseif (is_tag()) {single_tag_title();}
    elseif (is_day()) {echo"<li>Archive for "; the_time('F jS, Y'); echo'</li>';}
    elseif (is_month()) {echo"<li>Archive for "; the_time('F, Y'); echo'</li>';}
    elseif (is_year()) {echo"<li>Archive for "; the_time('Y'); echo'</li>';}
    elseif (is_author()) {echo"<li>Author Archive"; echo'</li>';}
    elseif (isset($_GET['paged']) && !empty($_GET['paged'])) {echo "<li>Blog Archives"; echo'</li>';}
    elseif (is_search()) {echo"<li>Search Results"; echo'</li>';}
    echo '</ul>';
}


//Gets post cat slug and looks for single-[cat slug].php and applies it
add_filter('single_template', create_function(
	'$the_template',
	'foreach( (array) get_the_category() as $cat ) {
		if ( file_exists(TEMPLATEPATH . "/single-{$cat->slug}.php") )
		return TEMPLATEPATH . "/single-{$cat->slug}.php"; }
	return $the_template;' )
);

function ie_style_sheets () {
	
	wp_register_style( 'ie8', get_stylesheet_directory_uri() . '/css/ie-lt8.css'  );
	$GLOBALS['wp_styles']->add_data( 'ie8', 'conditional', 'lte IE 8' );

	// wp_register_style( 'ie8', get_stylesheet_directory_uri() . '/ie-lt8.css'  );
	// $GLOBALS['wp_styles']->add_data( 'ie8', 'conditional', 'lte IE 8' );

	// wp_register_style( 'ie9', get_stylesheet_directory_uri() . '/ie-lt9.css'  );
	// $GLOBALS['wp_styles']->add_data( 'ie9', 'conditional', 'lte IE 9' );

	

	wp_enqueue_style( 'ie8' );
	// wp_enqueue_style( 'ie8' );
	// wp_enqueue_style( 'ie9' );
}

add_action ('wp_enqueue_scripts','ie_style_sheets');

// Galler template

 function custom_excerpt_length( $length ) {
	return 15;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );


/**
 * Implement the Custom Header feature.
 */
//require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Custom Post Types and Taxonomies
 */
require get_template_directory() . '/inc/post-types-taxonomies.php';
