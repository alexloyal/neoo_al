<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package neoo_al
 */
?>

	</div><!-- #content -->
</div><!-- #page -->
	<footer id="colophon" class="site-footer gradient background" role="contentinfo">
		<div class="site-info container">

			<div class="row">
				<?php if ( ! dynamic_sidebar( 'footer-widgets' ) ) : ?>
				<?php endif; // end sidebar widget area ?>
			</div>
			<div class="row">
				<?php wp_nav_menu(array(	
								'theme_location' => 'bottom',
								'menu_class' => 'inline-list menu col-md-12'

								)); ?>
			</div>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->


<?php wp_footer(); ?>
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X');ga('send','pageview');
        </script>
</body>
</html>
