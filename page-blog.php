<?php
/*
Template Name: Blog Template
*/
?>
<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package neoo_al
 */

get_header(); ?>
<!-- page-blog -->
	<div class="content-hero" style="background-image:url('<?php 
												echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); 
											 ?>');" class="">


	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	</div><!-- content hero -->

	<div class="inside-content">
			<div id="primary" class="content-area row">
				<main id="main" class="site-main row col-md-12" role="main">
		 
					<!-- query -->
							<?php 
							   
			  			 			$args = array (
										 
										'pagination'		=>true
									);
							  $wp_query = new WP_Query($args); 
							  $wp_query->query('&paged='.$paged);
							  $max = $wp_query->max_num_pages;
							  $paged = ( get_query_var('paged') > 1 ) ? get_query_var('paged') : 1;
							 
							 		wp_localize_script(
							 			'neoo_al_triggers',
							 			'ajax_posts',
							 			array(
							 				'startPage' => $paged,
							 				'maxPages' => $max,
							 				'nextLink' => next_posts($max, false)
							 			)
							 		); 

							  while ($wp_query->have_posts()) : $wp_query->the_post(); 
							?>

							 <?php 
							 
							 $format = get_post_format();
							 get_template_part( 'content', 'blog');

							 ?>

							 <!-- format <?php echo $format; ?> -->
							<?php endwhile; ?>
							
							<?php neoo_al_paging_nav(); ?>

							<?php 
							 
							  wp_reset_postdata();
							?>	
					 
				</main><!-- #main -->
			</div><!-- #primary -->

	</div><!-- .inside-content -->

<?php get_footer(); ?>
