<?php
/**
 * @package neoo_al
 */
?>
<!-- home-slider -->
<div class="home-slider-wrapper">
				<div id="home-slides" class="blueberry" data-home-slider="true" data-wrap="circular">
					<ul class="rslides">
					<?php 
					// WP_Query arguments
						$args = array (
						'post_type'              => 'homepage_slider',
						'post_status'            => 'published',
						'order'                  => 'ASC',
						'orderby'                => 'menu_order',
						);

						// The Query
						$hpSlidesQuery = new WP_Query( $args );

						if ($hpSlidesQuery->have_posts()) : 
						while ($hpSlidesQuery->have_posts()) : 
						$hpSlidesQuery->the_post(); 

					?>

						 
						<li class=""><?php echo get_the_post_thumbnail($post->ID, 'homepage-slide'); ?>
							<div class="slide-caption col-md-10 col-xs-8 centered">

								<h3><?php the_title(); ?></h3>
								<div class="col-md-12 subhead">
									<?php 
										$post_cta = get_post_meta( get_the_ID(), 'post-cta', true );
										$post_cta_link = get_post_meta( get_the_ID(), 'post-cta-link', true );
										// check if the custom field has a value
										if( ! empty( $post_cta ) || ! empty( $post_cta_link ) ) {
										  ?>  <a href="<?php echo $post_cta_link ?>" title="<?php the_title( ); ?>"> <?php echo $post_cta; ?></a> <?php 
										} else {
											?> <a href="<?php the_permalink(); ?>" title="<?php the_title( ); ?>">See more &raquo;</a> <?php
										} ?>
								</div>
								<span class="photo-credit">
								<?php 
										$photo_credit = get_post_meta( get_the_ID(), 'photo-credit', true );
										
										// check if the custom field has a value
										if( ! empty( $photo_credit ) || ! empty( $photo_credit_link ) ) {
										  ?> <?php echo $photo_credit; ?> <?php 
										} else {
											?> <?php
										} ?>
							</span>
							</div>
							

						</li>
						

					<?php endwhile; wp_reset_postdata();  ?>
							
					<?php endif; ?>
				</ul>
					 	


				 	</div>
				 	 
				 		
				</div>
	 		</div>
	  
