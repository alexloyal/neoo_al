<?php
/**
 * @package neoo_al
 */
?>
<!-- content-single -->
<article id="post-<?php the_ID(); ?>" <?php post_class('col-md-12'); ?>>
	<header class="entry-header col-md-12">
		<?php the_title( '<h1 class="entry-title single">', '</h1>' ); ?>

		<div class="entry-meta">
			<?php neoo_al_posted_on(); ?>
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->

	<div class="entry-content col-md-12">
		<?php if ( 'gallery' == get_post_format() ) : ?>
		
		<?php else: ?>
			<div class="col-md-3 social-share">

			<?php if(function_exists('kc_add_social_share')) kc_add_social_share(); ?>

		</div>

		<?php endif ?>

		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'neoo_al' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer col-md-12">
		<?php
			/* translators: used between list items, there is a space after the comma */
			$category_list = get_the_category_list( __( ', ', 'neoo_al' ) );

			/* translators: used between list items, there is a space after the comma */
			$tag_list = get_the_tag_list( '', __( ', ', 'neoo_al' ) );

			if ( ! neoo_al_categorized_blog() ) {
				// This blog only has 1 category so we just need to worry about tags in the meta text
				if ( '' != $tag_list ) {
					$meta_text = __( 'This entry was tagged %2$s. Bookmark the <a href="%3$s" rel="bookmark">permalink</a>.', 'neoo_al' );
				} else {
					$meta_text = __( 'Bookmark the <a href="%3$s" rel="bookmark">permalink</a>.', 'neoo_al' );
				}

			} else {
				// But this blog has loads of categories so we should probably display them here
				if ( '' != $tag_list ) {
					$meta_text = __( 'This entry was posted in %1$s and tagged %2$s. Bookmark the <a href="%3$s" rel="bookmark">permalink</a>.', 'neoo_al' );
				} else {
					$meta_text = __( 'This entry was posted in %1$s. Bookmark the <a href="%3$s" rel="bookmark">permalink</a>.', 'neoo_al' );
				}

			} // end check for categories on this blog

			printf(
				$meta_text,
				$category_list,
				$tag_list,
				get_permalink()
			);
		?>

		<?php edit_post_link( __( 'Edit', 'neoo_al' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-footer -->

	
</article><!-- #post-## -->
