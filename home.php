
<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package neoo_al
 */

get_header(); ?>
<!-- home -->
	<div id="primary" class="content-area container-fluid">
		<main id="main" class="site-main row" role="main">
			



			<?php get_template_part( 'home', 'slider' ); ?>

			 
			
		</main><!-- #main -->
	</div><!-- #primary -->

	<div id="" class="content-area container">

		<div class="home-bottom row">
			<aside class="home-blog-widget col-md-4">

			<h3 class="widget-title shadow">Recent Posts</h3>
			<?php
			// WP_Query arguments
			$args = array (
				'post_type'              => 'post',
				'posts_per_page'         => '3',
			);

			// The Query
			$blogSidebar = new WP_Query( $args );

			// The Loop
			if ( $blogSidebar->have_posts() ) {
				while ( $blogSidebar->have_posts() ) {
					$blogSidebar->the_post();
					// do something

					?>
					<div class="widget-post">
						<h3 class="sidebar-headline">
							<a href="<?php the_permalink(); ?>" title="<?php the_title( ); ?>">
							<?php the_title( ); ?>
							</a>
						</h3>	
						<?php

						echo get_the_post_thumbnail($post->ID, 'blog-thumb');

						the_excerpt(); ?>
					</div>	
					<?php
					
				}
			} else {
				// no posts found
			}

			// Restore original Post Data
			wp_reset_postdata(); ?>
			</aside>

			<?php if ( ! dynamic_sidebar( 'home-bottom' ) ) : ?>
			<?php endif; // end sidebar widget area ?>
		</div>

	</div><!-- #secondary -->

<?php get_footer(); ?>
