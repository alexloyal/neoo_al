/*

 Triggers for various jQuery pluggins
 Version 1.0 

 	-- by alej.co
*/


(function($) {


	 
	
	

	var windowWidth = $(window).width();	

	function SetSliderWidth(){
		 $('#home-slides').css('width', windowWidth);
	};

	$(document).ready(function(){

			//	SetSliderWidth();

			$('.search-icon').on('click', function(){
				$('#search').toggleClass('show');
			});

	});

// Smooth scroll	
	
	$(function() {
	  $('a[href*=#]:not([href=#])').click(function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
	      var target = $(this.hash);
	      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	      if (target.length) {
	        $('html,body').animate({
	          scrollTop: target.offset().top
	        }, 560);
	        return false;
	      }
	    }
	  });
	});

// Makes all menu items equal width.
	
	function setNavMenuWidth() {
		menuWidth = $('.top-tools').width();
		menuItemWidth = (menuWidth / 5.07);
		$('#menu-navigation.menu li a').css('width', menuItemWidth)
	};

	$(document).ready(function() {
		
		setNavMenuWidth();
		
	});

	$(window).resize(function() {
		
		setNavMenuWidth();
		
	});

	function setMenuWidth(){
		menuWidth = $('#menu-footer.menu').width();
		menuItemWidth = (menuWidth / 3.7);
		$('#menu-footer.menu li a').css('width', menuItemWidth)
	};

	$(document).ready(function() {
		
			setMenuWidth();
		
	});

	$(window).resize(function() {
			setMenuWidth();
		
	});



// Back to top script
	 
	 $(document).ready(function() {
				 $('#backToTop').hide();
				 
				 $(function () {
					 $(window).scroll(function() {
						 if ($(this).scrollTop() > 400){
							 $('#backToTop').fadeIn();
							 // $('#masthead').addClass('fixed'); 
						 } else {
							 $('#backToTop').fadeOut();
							 // $('#masthead').removeClass('fixed'); 
						 }				 
					 });

					 	/* $(window).scroll(function(){
							var scrollTop = $(window).scrollTop();
							if (scrollTop >= 420) {
							
							$('#secondary-navigation').fadeIn();
						 	$('#secondary-navigation').css('top','0%');
						 	$('#secondary-navigation').addClass('fixed');
						 	// $('#secondary-navigation').css('width', windowWidth)
							} else if (scrollTop <= 419) {
								$('#secondary-navigation').fadeOut();
						 	$('#secondary-navigation').css('top','-25%');
						 	$('#secondary-navigation').removeClass('fixed')
							}
						}); */





					 
					 $('#backToTop a').click(function(){
						 $('body,html').animate({
							 scrollTop:0
						 }, 800);
						 return false;
					 });
				 });
	 });
 
  
 


/* Slides JS */

function SetSlideHeight(){
		
		$(window).load(function(){
			
			var slideHeight = ($(window).height())-($('#masthead').height());

		 
		 $('.rslides li').css('height', slideHeight);
		 //$('.content-hero').css('min-height', slideHeight);
		 $('#home-slides').css('height', slideHeight);
		}); 
};

$(function() {
 		$(function() {
		    $(".rslides").responsiveSlides({
		    				auto:false,
		    				pager:true,
		    				nav: true,
		    				speed:600,
		    				timeout: 8000,
		    				prevText: "&nbsp;",
		    				nextText:"&nbsp;",
		    				before: function(){

		    						$(window).load(function(){
		    							$('#home-slides').fadeIn('slow');
		    						});


		    				},
		    });

		 	SetSlideHeight();
		  });
    });

	$(window).resize(function(){

		 SetSlideHeight();
	});



// Adding color box to gallery links.

$(function(){
<<<<<<< HEAD
		$('.gallery a').colorbox({rel:'gallery a',transition:"fade",width:"100%", height:"100%",innerWidth:"100%"});
=======
<<<<<<< HEAD
		$('.gallery a').colorbox({rel:'gallery a',transition:"fade",width:"80%", height:"80%"});
=======
		$('.gallery a').colorbox({rel:'gallery a',transition:"fade",width:"100%", height:"100%",innerWidth:"100%"});
>>>>>>> e4c4268b24063acf5df31bb1ab62692c3c92742e
>>>>>>> 2343b5caac82c45a7f1c93d83421181f0d7060c9

		});

	
		$(document).ready(function(){
			$('.navbar').addClass('show').delay(600);
			$('.home-slider-wrapper').addClass('show').delay(500);
			
		});


	
 
})(jQuery);

if ($('body').is('.page-template-page-blog-php,.search-results')) {
    (function() {

 
    var pageNum = parseInt(ajax_posts.startPage) + 1;
    var max = parseInt(ajax_posts.maxPages);
    var nextLink = ajax_posts.nextLink;
 
    if(pageNum <= max) {
     
        $('#main')
            .append('<div class="ajax-posts-placeholder-'+ pageNum +'"></div>')
            .append('<p id="ajax-posts-load-posts"><a href="#" class="arrow arrow-down2-icon"></a></p>');
            
       
        $('.paging-navigation').remove();
    }
    
    
  
    $('#ajax-posts-load-posts a').click(function() {
    
        // Are there more posts to load?
        if(pageNum <= max) {
        	
        
            if ($(this).hasClass('rotate')) {

            	
            	$(this).addClass('again');
            };  

            if ($(this).hasClass('again')) {
            	

            	$(this).removeClass('again');
            } else {

            	$(this).addClass('rotate')
            };
            
            $('.ajax-posts-placeholder-'+ pageNum).load(nextLink + ' .post',
                function() {
                    
                    pageNum++;
                    nextLink = nextLink.replace(/\/page\/[0-9]?/, '/page/'+ pageNum);
                    
                   
                    $('#ajax-posts-load-posts')
                        .before('<div class="ajax-posts-placeholder-'+ pageNum +'"></div>')
                    
                    // Update the button message.
                    if(pageNum <= max) {
                        $('#ajax-posts-load-posts a').text(' ');
                    } else {
                        $('#ajax-posts-load-posts a').text(' ');
                    }
                }
            );
        } else {
            $('#ajax-posts-load-posts a').append('.');
        }   
        
        return false;
    });

	

    })(jQuery);
};

 
	

 